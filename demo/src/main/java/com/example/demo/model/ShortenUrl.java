package com.example.demo.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "shorten_url")
public class ShortenUrl {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "shorten_url_id")
    private int shortenUrlId;

    @Column(name = "original_url")
    private String originalUrl;

    @Column(name = "created_time")
    private Timestamp createdTime;

    public int getShortenUrlId() {
        return shortenUrlId;
    }

    public void setShortenUrlId(int shortenUrlId) {
        this.shortenUrlId = shortenUrlId;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public Timestamp getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(Timestamp createdTime) {
        this.createdTime = createdTime;
    }
}
