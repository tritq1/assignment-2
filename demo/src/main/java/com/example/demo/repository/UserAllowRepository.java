package com.example.demo.repository;

import com.example.demo.model.UserAllow;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserAllowRepository extends JpaRepository<UserAllow, Integer> {
}
