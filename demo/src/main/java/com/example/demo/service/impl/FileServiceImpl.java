package com.example.demo.service.impl;

import com.example.demo.config.UserLoginContext;
import com.example.demo.domain.FileCommonResponseDto;
import com.example.demo.domain.FileDto;
import com.example.demo.domain.FileResponseDto;
import com.example.demo.model.FileModel;
import com.example.demo.model.User;
import com.example.demo.repository.FileRepository;
import com.example.demo.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import static com.example.demo.constants.Constants.FILE_NOT_FOUND;
import static com.example.demo.constants.Constants.USER_HOME;

@Service
public class FileServiceImpl implements FileService {
    @Value("${file.upload-dir}")
    private String uploadDir;

    @Autowired
    private FileRepository fileRepository;

    /**
     * Function store file to directory on server and insert file name into database.
     * @param file
     * @return
     */
    @Override
    public ResponseEntity storeFile(MultipartFile file) {
        String storeDirectory = System.getProperty(USER_HOME) + uploadDir + File.separator + UserLoginContext.getUserName();
        FileCommonResponseDto fileCommonResponseDto = new FileCommonResponseDto();
        try{
            Files.createDirectories(Paths.get(storeDirectory));
            Path fileUpload = Paths.get(storeDirectory).resolve(Objects.requireNonNull(file.getOriginalFilename()));
            Files.copy(file.getInputStream(), fileUpload, StandardCopyOption.REPLACE_EXISTING);

            //set response
            fileCommonResponseDto.setStatus(true);
            fileCommonResponseDto.setMessage("Upload successfully with file name: " + Objects.requireNonNull(file.getOriginalFilename()));

            FileModel userFileModel = fileRepository
                    .getUserFileModelByFileNameAndUserId(Objects.requireNonNull(file.getOriginalFilename()), UserLoginContext.getUserDid());
            //file name is existed in DB -> return success
            if (userFileModel != null) {
                return new ResponseEntity(fileCommonResponseDto, HttpStatus.OK);
            }

            //get user details
            User userModel = new User();
            userModel.setUserId(UserLoginContext.getUserDid());

            //store file name to database if file is not existed.
            userFileModel = new FileModel();
            userFileModel.setFileName(Objects.requireNonNull(file.getOriginalFilename()));
            userFileModel.setUser(userModel);
            fileRepository.save(userFileModel);

            return new ResponseEntity(fileCommonResponseDto, HttpStatus.OK);
        } catch (IOException ex) {
            fileCommonResponseDto.setStatus(false);
            fileCommonResponseDto.setMessage("Failed upload with file name: " + file.getName());

            return new ResponseEntity(fileCommonResponseDto, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Function get file from service with file name and user id, if file not exist, throw exception.
     * @param userId
     * @param fileName
     * @return resource
     * @throws Exception
     */
    @Override
    public Resource downloadFile(int userId, String fileName) throws Exception {
        String storeDirectory = System.getProperty(USER_HOME) + uploadDir + File.separator + UserLoginContext.getUserName();

        FileModel userFileModel = fileRepository.getUserFileModelByFileNameAndUserId(fileName, userId);

        if (userFileModel == null) {
            throw new FileNotFoundException(FILE_NOT_FOUND + fileName);
        }

        String fileUri = storeDirectory + File.separator + userFileModel.getFileName();

        try {
            Resource resource = new UrlResource(Paths.get(fileUri).toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
            }
        } catch (MalformedURLException e) {
            throw new FileNotFoundException(FILE_NOT_FOUND + fileUri);
        }
    }

    @Override
    public FileResponseDto getListFileOfUser() {
        User user = new User();
        user.setUserId(UserLoginContext.getUserDid());
        List<FileModel> listFile = fileRepository.getAllByUser(user);

        List<FileDto> fileDtos = new ArrayList<>();
        for (FileModel fileModel: listFile) {
            FileDto fileDto = new FileDto();
            fileDto.setFileId(fileModel.getFileId());
            fileDto.setFileName(fileModel.getFileName());
            fileDtos.add(fileDto);
        }

        FileResponseDto fileResponseDto = new FileResponseDto();
        fileResponseDto.setData(fileDtos);
        return fileResponseDto;
    }
}
