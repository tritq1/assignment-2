package com.example.demo.service;

import com.example.demo.domain.LoginRequestDto;
import com.example.demo.model.User;

public interface UserService {
    User createUser(LoginRequestDto requestDto);
}
