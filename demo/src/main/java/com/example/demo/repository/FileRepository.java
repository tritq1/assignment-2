package com.example.demo.repository;

import com.example.demo.model.FileModel;
import com.example.demo.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface FileRepository extends JpaRepository<FileModel, Integer> {
    @Query("SELECT uf FROM FileModel uf WHERE uf.fileName = :fileName and uf.user.userId = :userId")
    FileModel getUserFileModelByFileNameAndUserId(String fileName, int userId);
    List<FileModel> getAllByUser(User user);
}
