package com.example.demo.repository;

import com.example.demo.model.ShortenUrl;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ShortenUrlRepository extends JpaRepository<ShortenUrl, Integer> {
}
