package com.example.demo.model;

import javax.persistence.*;

@Entity
@Table(name = "user_allow")
public class UserAllow {
    @Column(name = "user_allow_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    private int userAllowId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_allowed_id")
    private User userAllowed;

    public int getUserAllowId() {
        return userAllowId;
    }

    public void setUserAllowId(int userAllowId) {
        this.userAllowId = userAllowId;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public User getUserAllowed() {
        return userAllowed;
    }

    public void setUserAllowed(User userAllowed) {
        this.userAllowed = userAllowed;
    }
}
